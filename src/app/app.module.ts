import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SliderComponent } from './slider/slider.component';
import { MenuComponent } from './menu/menu.component';
import { BotonFlotanteComponent } from './boton-flotante/boton-flotante.component';
import { FooterComponent } from './footer/footer.component';
import { DropdownMenuComponent } from './dropdown-menu/dropdown-menu.component';
import { AdmisionComponent } from './admision/admision.component';
import { AdmisionCostoComponent } from './admision-costo/admision-costo.component';
import { AdmisionRequisitosComponent } from './admision-requisitos/admision-requisitos.component';
import { HeaderBaseComponent } from './header-base/header-base.component';
import { MainMenuComponent } from './main-menu/main-menu.component';
import { LogosComponent } from './logos/logos.component';
import { SocialMediaComponent } from './social-media/social-media.component';
import { SubTitulosComponent } from './sub-titulos/sub-titulos.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { HeaderEstudiantesComponent } from './header-estudiantes/header-estudiantes.component';
import { PracticasComponent } from './practicas/practicas.component';
import { HeaderPracticasComponent } from './header-practicas/header-practicas.component';
import { HeaderProyeccionComponent } from './header-proyeccion/header-proyeccion.component';
import { ProyeccionComponent } from './proyeccion/proyeccion.component';
import { BienestarComponent } from './bienestar/bienestar.component';
import { HeaderBienestarComponent } from './header-bienestar/header-bienestar.component';
import { HeaderDocentesComponent } from './header-docentes/header-docentes.component';
import { DocentesComponent } from './docentes/docentes.component';
import { InvestigacionComponent } from './investigacion/investigacion.component';
import { HeaderInvestigacionComponent } from './header-investigacion/header-investigacion.component';
import { RevistaComponent } from './revista/revista.component';
import { HeaderRevistaComponent } from './header-revista/header-revista.component';
import { HeaderTramitesComponent } from './header-tramites/header-tramites.component';
import { TramitesComponent } from './tramites/tramites.component';
import { PracticasCuadrosComponent } from './practicas-cuadros/practicas-cuadros.component';
import { SocialMediaFooterComponent } from './social-media-footer/social-media-footer.component';
import { ErrorNotFoundComponent } from './error-not-found/error-not-found.component';
import { HeaderNotFoundComponent } from './header-not-found/header-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SliderComponent,
    MenuComponent,
    BotonFlotanteComponent,
    FooterComponent,
    DropdownMenuComponent,
    AdmisionComponent,
    AdmisionCostoComponent,
    AdmisionRequisitosComponent,
    HeaderBaseComponent,
    MainMenuComponent,
    LogosComponent,
    SocialMediaComponent,
    SubTitulosComponent,
    EstudiantesComponent,
    HeaderEstudiantesComponent,
    PracticasComponent,
    HeaderPracticasComponent,
    HeaderProyeccionComponent,
    ProyeccionComponent,
    BienestarComponent,
    HeaderBienestarComponent,
    HeaderDocentesComponent,
    DocentesComponent,
    InvestigacionComponent,
    HeaderInvestigacionComponent,
    RevistaComponent,
    HeaderRevistaComponent,
    HeaderTramitesComponent,
    TramitesComponent,
    PracticasCuadrosComponent,
    SocialMediaFooterComponent,
    ErrorNotFoundComponent,
    HeaderNotFoundComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
