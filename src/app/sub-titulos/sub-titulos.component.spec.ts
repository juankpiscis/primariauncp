import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubTitulosComponent } from './sub-titulos.component';

describe('SubTitulosComponent', () => {
  let component: SubTitulosComponent;
  let fixture: ComponentFixture<SubTitulosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubTitulosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubTitulosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
