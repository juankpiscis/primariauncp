import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'sub-titulos',
  templateUrl: './sub-titulos.component.html',
  styleUrls: ['./sub-titulos.component.scss']
})
export class SubTitulosComponent implements OnInit {

  public SubTitulo: string = '';
  public Descripcion: string = '';
  constructor() { }

  @Input() set subtitulo(_val: string){
    this.SubTitulo = _val;
  } 
  @Input() set descripcion(_val: string){
    this.Descripcion = _val;
  } 
  

  ngOnInit(): void {
  }

}
