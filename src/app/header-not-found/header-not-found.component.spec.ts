import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderNotFoundComponent } from './header-not-found.component';

describe('HeaderNotFoundComponent', () => {
  let component: HeaderNotFoundComponent;
  let fixture: ComponentFixture<HeaderNotFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderNotFoundComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
