import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'header-not-found',
  templateUrl: './header-not-found.component.html',
  styleUrls: ['./header-not-found.component.scss']
})
export class HeaderNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
