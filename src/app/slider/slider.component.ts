import { Component, OnInit } from '@angular/core';
import { NgbCarousel, NgbSlideEvent, NgbSlideEventSource } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  public images = ['../../../../assets/img/banner_base.png', '../../assets/img/banner_base2.png', '../../assets/img/banner_base3.png']

  constructor() { }

  ngOnInit(): void {
  }

}
