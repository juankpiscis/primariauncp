export interface MenuNav{
    lOpcion:boolean,
    cNombre:string,
    cUrl?:string,
    hijos?: MenuNav[],
    lSeleccionado: boolean | false
}