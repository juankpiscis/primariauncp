import { isPlatformBrowser } from '@angular/common';
import { Component, ElementRef, OnInit, ViewChild, Renderer2, HostListener, Inject, PLATFORM_ID } from '@angular/core';

@Component({
  selector: 'main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss']
})
export class MainMenuComponent implements OnInit {

  constructor(private renderer: Renderer2, @Inject(PLATFORM_ID) private platformID: Object) { }
  @ViewChild('header') header: ElementRef | undefined;
  @ViewChild('myNav') element: ElementRef | undefined;
  @ViewChild('burguer') burguer: ElementRef | undefined;


  ngOnInit(): void {

    if(isPlatformBrowser(this.platformID)){
      if(this.header){
        console.log(this.header);
        if(document.body.scrollTop > 100){
          this.renderer.addClass(this.header?.nativeElement,'header-scrolled');
        }else{
          this.renderer.removeClass(this.header?.nativeElement,'header-scrolled');
        }
      }  
    }          
  }

  @HostListener("window:scroll", []) onWindowScroll() {

    if(isPlatformBrowser(this.platformID)){
      // do some stuff here when the window is scrolled
    if(document.body.scrollTop > 100){
      this.renderer.addClass(this.header?.nativeElement,'header-scrolled');
    }else{
      this.renderer.removeClass(this.header?.nativeElement,'header-scrolled');
    }
    }
    
  }


  ChangeStateNavMobile(){

    let isMovil: boolean = this.element?.nativeElement.classList.contains('navbar-mobile');
    let isBars: boolean = this.burguer?.nativeElement.classList.contains('fa-bars');
    if(isMovil){
      this.renderer.removeClass(this.element?.nativeElement, 'navbar-mobile');
    }else{
      this.renderer.addClass(this.element?.nativeElement, 'navbar-mobile');
    }

    if(isBars){
      this.renderer.removeClass(this.burguer?.nativeElement, 'fa-bars');
      this.renderer.addClass(this.burguer?.nativeElement, 'fa-times');
    }else{
      this.renderer.removeClass(this.burguer?.nativeElement, 'fa-times');
      this.renderer.addClass(this.burguer?.nativeElement, 'fa-bars');
    }
  }

}
