import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'boton-flotante',
  templateUrl: './boton-flotante.component.html',
  styleUrls: ['./boton-flotante.component.scss']
})
export class BotonFlotanteComponent implements OnInit {

  public lMostrar: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }
  OcultarBoton(){
    this.lMostrar = false;
  }

}
