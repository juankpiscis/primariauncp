import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderBienestarComponent } from './header-bienestar.component';

describe('HeaderBienestarComponent', () => {
  let component: HeaderBienestarComponent;
  let fixture: ComponentFixture<HeaderBienestarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderBienestarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderBienestarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
