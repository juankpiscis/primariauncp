import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTramitesComponent } from './header-tramites.component';

describe('HeaderTramitesComponent', () => {
  let component: HeaderTramitesComponent;
  let fixture: ComponentFixture<HeaderTramitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderTramitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderTramitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
