import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'header-base',
  templateUrl: './header-base.component.html',
  styleUrls: ['./header-base.component.scss']
})
export class HeaderBaseComponent implements OnInit {

  public cRutaImagen: string = "";
  constructor() { }

  @Input() set imagensrc(_val: string){
    this.cRutaImagen = "url('./././assets/img/" + _val + "');" ;
    console.log(this.cRutaImagen);
  }

  ngOnInit(): void {
  }

}
