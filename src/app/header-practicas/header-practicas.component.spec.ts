import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderPracticasComponent } from './header-practicas.component';

describe('HeaderPracticasComponent', () => {
  let component: HeaderPracticasComponent;
  let fixture: ComponentFixture<HeaderPracticasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderPracticasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderPracticasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
