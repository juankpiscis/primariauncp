import { NgModule } from '@angular/core';
import { Router, RouterModule, Routes } from '@angular/router';
import { AdmisionCostoComponent } from './admision-costo/admision-costo.component';
import { AdmisionComponent } from './admision/admision.component';
import { AdmisionRequisitosComponent } from './admision-requisitos/admision-requisitos.component';
import { HomeComponent } from './home/home.component';
import { EstudiantesComponent } from './estudiantes/estudiantes.component';
import { PracticasComponent } from './practicas/practicas.component';
import { DocentesComponent } from './docentes/docentes.component';
import { ProyeccionComponent } from './proyeccion/proyeccion.component';
import { BienestarComponent } from './bienestar/bienestar.component';
import { InvestigacionComponent } from './investigacion/investigacion.component';
import { RevistaComponent } from './revista/revista.component';
import { TramitesComponent } from './tramites/tramites.component';
import { ErrorNotFoundComponent } from './error-not-found/error-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent    
  },
  {
    path: 'admision',
    component: AdmisionComponent
  },
  {
    path: 'estudiantes',
    component: EstudiantesComponent
  },
  {
    path: 'practicas',
    component: PracticasComponent
  },
  {
    path: 'docentes',
    component: DocentesComponent
  },
  {
    path: 'proyeccion',
    component: ProyeccionComponent
  },
  {
    path: 'bienestar',
    component: BienestarComponent
  },
  {
    path: 'investigacion',
    component: InvestigacionComponent
  },
  {
    path: 'revista',
    component: RevistaComponent
  }
  ,
  {
    path: 'tramites',
    component: TramitesComponent
  },
  {
    path: '**',
    component: ErrorNotFoundComponent
  }
  

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabledBlocking'
})
  ],
  exports:[
    RouterModule
  ]
})
export class AppRoutingModule { }
