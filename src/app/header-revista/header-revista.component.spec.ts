import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderRevistaComponent } from './header-revista.component';

describe('HeaderRevistaComponent', () => {
  let component: HeaderRevistaComponent;
  let fixture: ComponentFixture<HeaderRevistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderRevistaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderRevistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
