import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderEstudiantesComponent } from './header-estudiantes.component';

describe('HeaderEstudiantesComponent', () => {
  let component: HeaderEstudiantesComponent;
  let fixture: ComponentFixture<HeaderEstudiantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderEstudiantesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderEstudiantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
