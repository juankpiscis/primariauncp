import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderInvestigacionComponent } from './header-investigacion.component';

describe('HeaderInvestigacionComponent', () => {
  let component: HeaderInvestigacionComponent;
  let fixture: ComponentFixture<HeaderInvestigacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderInvestigacionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderInvestigacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
