import { Component, OnInit } from '@angular/core';
import { MenuNav } from '../models/menu.model';

@Component({
  selector: 'dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.scss']
})
export class DropdownMenuComponent implements OnInit {

  public listaMenu: MenuNav[] = this.ConstruirMenu();
  constructor() { }

  ngOnInit(): void {
  }
  ConstruirMenu(): MenuNav[]{
    return [
      {
        lOpcion: true,  
        cNombre: 'Inicio',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: false,
        cNombre: 'Pregrado',
        lSeleccionado: false,
        hijos: [
          {
            lOpcion:true,
            cNombre: 'Admisión',
            cUrl: '#',
            lSeleccionado:false
          },
          {
            lOpcion:true,
            cNombre: 'Estudiantes',
            cUrl: '#',
            lSeleccionado:false
          }
        ]
      },
      {
        lOpcion: true,
        cNombre: 'Docentes',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: true,
        cNombre: 'Trámites',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: false,
        cNombre: 'Investigación',
        lSeleccionado: false,
        hijos:[
          {
            lOpcion: true,
            cNombre: 'Directivas',
            cUrl: '#',
            lSeleccionado:false
          },
          {
            lOpcion: true,
            cNombre: 'Revista',
            cUrl: '#',
            lSeleccionado:false
          }
        ]
      },
      {
        lOpcion: true,
        cNombre: 'Campus Virtual',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: true,
        cNombre: 'Noticias',
        cUrl: '#',
        lSeleccionado:false
      }

    ]
  }

}
