import { Component, OnInit } from '@angular/core';
import { MenuNav } from '../models/menu.model';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public listaMenu: MenuNav[] = this.ConstruirMenu();
  public ItemAct: MenuNav | undefined;
  public lMostrarMenu: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }
  ConstruirMenu(): MenuNav[]{
    return [
      {
        lOpcion: true,  
        cNombre: 'Inicio',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: false,
        cNombre: 'Admisión',
        lSeleccionado: false,
        hijos: [
          {
            lOpcion:true,
            cNombre: 'Modalidades',
            cUrl: 'admision-modalidades',
            lSeleccionado:false
          },
          {
            lOpcion:true,
            cNombre: 'Costo de Inscripción',
            cUrl: 'admision-costo',
            lSeleccionado:false
          },
          {
            lOpcion:true,
            cNombre: 'Requisitos',
            cUrl: 'admision-requisitos',
            lSeleccionado:false
          }
        ]
      },
      {
        lOpcion: false,
        cNombre: 'Pregrado',
        lSeleccionado: false,
        hijos: [
          {
            lOpcion:true,
            cNombre: 'Admisión',
            cUrl: '#',
            lSeleccionado:false
          },
          {
            lOpcion:true,
            cNombre: 'Estudiantes',
            cUrl: '#',
            lSeleccionado:false
          }
        ]
      },
      {
        lOpcion: true,
        cNombre: 'Docentes',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: true,
        cNombre: 'Trámites',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: false,
        cNombre: 'Investigación',
        lSeleccionado: false,
        hijos:[
          {
            lOpcion: true,
            cNombre: 'Directivas',
            cUrl: '#',
            lSeleccionado:false
          },
          {
            lOpcion: true,
            cNombre: 'Revista',
            cUrl: '#',
            lSeleccionado:false
          }
        ]
      },
      {
        lOpcion: true,
        cNombre: 'Campus Virtual',
        cUrl: '#',
        lSeleccionado:false
      },
      {
        lOpcion: true,
        cNombre: 'Noticias',
        cUrl: '#',
        lSeleccionado:false
      }

    ]
  }
  SeleccionarItem(itm: MenuNav){
    if(itm == this.ItemAct){
      this.ItemAct = undefined;
      itm.lSeleccionado = false;
      return;
    }
    this.listaMenu.forEach(element => {
      element.lSeleccionado=false;
    });
    itm.lSeleccionado = true;
    this.ItemAct = itm;
  }
  MostrarOcultarMenu(){
    this.lMostrarMenu = !this.lMostrarMenu;
  }
}
