import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderDocentesComponent } from './header-docentes.component';

describe('HeaderDocentesComponent', () => {
  let component: HeaderDocentesComponent;
  let fixture: ComponentFixture<HeaderDocentesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderDocentesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderDocentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
