import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmisionCostoComponent } from './admision-costo.component';

describe('AdmisionCostoComponent', () => {
  let component: AdmisionCostoComponent;
  let fixture: ComponentFixture<AdmisionCostoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmisionCostoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmisionCostoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
