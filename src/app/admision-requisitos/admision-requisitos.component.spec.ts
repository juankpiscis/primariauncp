import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdmisionRequisitosComponent } from './admision-requisitos.component';

describe('AdmisionRequisitosComponent', () => {
  let component: AdmisionRequisitosComponent;
  let fixture: ComponentFixture<AdmisionRequisitosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdmisionRequisitosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdmisionRequisitosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
