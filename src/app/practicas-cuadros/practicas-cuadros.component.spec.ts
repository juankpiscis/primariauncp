import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticasCuadrosComponent } from './practicas-cuadros.component';

describe('PracticasCuadrosComponent', () => {
  let component: PracticasCuadrosComponent;
  let fixture: ComponentFixture<PracticasCuadrosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PracticasCuadrosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticasCuadrosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
