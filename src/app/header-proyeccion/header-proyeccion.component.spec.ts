import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderProyeccionComponent } from './header-proyeccion.component';

describe('HeaderProyeccionComponent', () => {
  let component: HeaderProyeccionComponent;
  let fixture: ComponentFixture<HeaderProyeccionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderProyeccionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderProyeccionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
